const Controller = require('./controller.js');
const Model = require('./model.js');
const Routes = require('./routes.js');

module.exports = {
  controller: Controller,
  model: Model,
  routes: Routes,
};
